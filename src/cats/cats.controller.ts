import { Controller, Get } from '@nestjs/common';

@Controller()
export class CatsController {

    @Get('/cats')
    getCats(){
        return ['Eleven', 'Choupisson']
    }
}
